# Fit House
Simple gym management app.

## Changelog
- v0.2 Complete user registration & login, including basic form validation & JWT authentication. 
- v0.1 Project initialization.

### Installation and running: 

Clone this repository.<br />
Install packages: `npm install`<br /> 
Run application: `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
