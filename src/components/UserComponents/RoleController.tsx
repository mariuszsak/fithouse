import React from 'react';
import AuthService from '../../helpers/AuthService';
import {UserRole} from './User';

export interface ForProps {
    for: UserRole[] | UserRole;
}

export class RoleController extends React.Component<ForProps, {}> {
    private readonly _authService: AuthService = new AuthService();

    render(): JSX.Element | undefined {
        if ((this.props.for === this._authService.getRoleFromToken())
            || this.props.for.includes(this._authService.getRoleFromToken() || 'user')) {
            return <>{this.props.children}</>;
        } else {
            return <></>;
        }
    }
}

export default RoleController;
