import React from 'react';
import axios from 'axios';
import autobind from 'autobind-decorator';
import {
    Button,
    Col,
    Container,
    Form,
    Row
} from 'react-bootstrap';

export interface RegisterComponentProps {
    history: {
        push(url: string): void;
    };
}

export interface RegisterComponentState {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export class RegisterComponent extends React.Component<RegisterComponentProps, RegisterComponentState> {
    private static readonly NAME_REGEXP: RegExp = /^([a-zA-Z]+')*[a-zA-Z]+$/;
    private static readonly MAIL_REGEXP: RegExp = /^([a-zA-Z0-9_+]+\.)*[a-zA-Z0-9_+]+@([a-zA-Z0-9_+]+\.)+[a-zA-Z0-9]{2,3}$/;

    constructor(props: RegisterComponentProps) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        };
    }

    render(): JSX.Element {
        return (
            <Container className='centerHV'>
                <Row>
                    <Col/>
                    <Col>
                        <Form>
                            <Form.Group controlId='firstName'>
                                <Form.Label>First Name:</Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='your first name'
                                    onChange={this.updateFirstName}
                                />
                            </Form.Group>
                            <Form.Group controlId='lastName'>
                                <Form.Label>Last Name:</Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='your last name'
                                    onChange={this.updateLastName}
                                />
                            </Form.Group>
                            <Form.Group controlId='password'>
                                <Form.Label>Set Password:</Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='your password'
                                    onChange={this.updatePassword}
                                />
                            </Form.Group>
                            <Form.Group controlId='email'>
                                <Form.Label>Email:</Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='type your email'
                                    onChange={this.updateEmail}
                                />
                            </Form.Group>
                            <Form.Group controlId='button'>
                                <Button
                                    disabled={
                                        !RegisterComponent.NAME_REGEXP.test(this.state.firstName)
                                        || !RegisterComponent.NAME_REGEXP.test(this.state.lastName)
                                        || this.state.password.length < 5
                                        || !RegisterComponent.MAIL_REGEXP.test(this.state.email)
                                    }
                                    variant='primary'
                                    type='submit'
                                    onClick={this.register}>
                                    REGISTER
                                </Button>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col/>
                </Row>
            </Container>
        );
    }

    @autobind
    private updateFirstName(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            firstName: event.currentTarget.value
        });
    }

    @autobind
    private updateLastName(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            lastName: event.currentTarget.value
        });
    }

    @autobind
    private updatePassword(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            password: event.currentTarget.value
        });
    }

    @autobind
    private updateEmail(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            email: event.currentTarget.value
        });
    }

    @autobind
    private register(): void {
        const REGISTER_ENDPOINT = 'http://localhost:8080/api/register.php';
        axios.post(REGISTER_ENDPOINT, JSON.stringify(this.state), {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(response => {
            if (response.status === 200) {
                this.props.history.push('/registrationSuccess');
            }
        }).catch(error => {
            console.log(error);
        });
    }
}

export default RegisterComponent;
