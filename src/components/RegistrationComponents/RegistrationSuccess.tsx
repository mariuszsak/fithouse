import React from 'react';
import {Link} from 'react-router-dom';

export class RegistrationSuccess extends React.Component<{}, {}> {
    render(): JSX.Element {
        return (
            <div>
                <p>
                    Please <Link to={'/'}>login to Fit House</Link>.
                </p>
            </div>
        );
    }
}

export default RegistrationSuccess;
