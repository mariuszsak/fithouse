import React from 'react';
import autobind from 'autobind-decorator';

export class LeftMenuUser extends React.Component<{}, {}> {

    render(): JSX.Element {
        return (
            <div>
                menu here
            </div>
        );
    }

    @autobind
    private handleItemClick(e: React.MouseEvent<HTMLAnchorElement>) {
        //temporarily disabled
    }
}

export default LeftMenuUser;
