import React from 'react';
import axios from 'axios';
import AuthService from '../helpers/AuthService';
import CookieService from '../helpers/CookieService';
import {Link} from 'react-router-dom';
import autobind from 'autobind-decorator';
import AppContext from '../helpers/AppContext';
import {
    Button,
    Col,
    Container,
    Form,
    FormControl,
    Row
} from 'react-bootstrap';

export interface LoginPageProps {
    history: {
        push(url: string): void;
    };
}

export interface LoginPageState {
    username: string;
    password: string;
}

export class LoginPage extends React.Component<LoginPageProps, LoginPageState> {
    private readonly _authService: AuthService = new AuthService();
    private readonly _cookieService: CookieService = new CookieService();

    constructor(props: LoginPageProps) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentDidMount(): void {
        if (this._authService.getLocalStorageToken() && !this._authService.isLocalStorageTokenExpired(this._authService.getLocalStorageToken())) {
            this._cookieService.setCookie();
            this.props.history.push('/dashboard');
        }
        if (this._authService.isLocalStorageTokenExpired(this._authService.getLocalStorageToken())) {
            this._authService.logout();
        }
    }

    render(): JSX.Element {
        return (
            <Container className='centerHV'>
                <Row>
                    <Col/>
                    <Col>
                        <Form.Group controlId='formUsername'>
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='username'
                                onChange={this.updateUsernameData}
                            />
                        </Form.Group>
                        <Form.Group controlId='formPassword'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                placeholder='password'
                                onChange={this.updatePasswordData}
                            />
                        </Form.Group>
                        <Button
                            variant='primary'
                            type="submit"
                            block
                            onClick={this.login}>
                            LOGIN
                        </Button>
                        <Form.Group controlId='register'>
                            New to us? <Link to='/register'>Register</Link>
                        </Form.Group>
                    </Col>
                    <Col/>
                </Row>
            </Container>
        );
    }

    @autobind
    private login(): void {
        const LOGIN_ENDPOINT: string = 'http://localhost:8080/login.php';
        axios.post(LOGIN_ENDPOINT, JSON.stringify(this.state), {
            withCredentials: true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        }).then(response => {
            if (!response) {
                alert('response');
            }
            if (response.data.jwt) {
                this._authService.setLocalStorageToken(response.data.jwt);
                this._authService.setRoleFromToken(response.data.jwt);
                this.props.history.push('/dashboard');
            }
        }).catch(error => {
            this.props.history.push('/loginError');
            console.log(error);
        });
    }

    @autobind
    private updateUsernameData(event: React.ChangeEvent<FormControl & HTMLInputElement>): void {
        event.preventDefault();
        this.setState({
            username: event.currentTarget.value
        });
    }

    @autobind
    private updatePasswordData(event: React.ChangeEvent<FormControl & HTMLInputElement>): void {
        event.preventDefault();
        this.setState({
            password: event.currentTarget.value
        });
    }
}

LoginPage.contextType = AppContext;
export default LoginPage;
