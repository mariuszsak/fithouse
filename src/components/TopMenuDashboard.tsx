import React from 'react';
import AuthService from '../helpers/AuthService';
import autobind from 'autobind-decorator';

export class TopMenuDashboard extends React.Component<{}, {}> {
    private readonly _auth = new AuthService();

    render(): JSX.Element {
        return (
            <div className='menudashdiv'>
                <button onClick={this.handleItemClick}>Logout</button>
            </div>
        );
    }

    @autobind
    private handleItemClick(): void {
        this._auth.logout();
    }
}

export default TopMenuDashboard;
