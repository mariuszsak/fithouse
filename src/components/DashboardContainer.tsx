import React from 'react';
import LeftDashboard from './LeftDashboard';
import RightDashboard from './RightDashboard';
import TopMenuDashboard from './TopMenuDashboard';

export class DashboardContainer extends React.Component<{}, {}> {
    render(): JSX.Element {
        return (
            <div>
                <TopMenuDashboard/>
                <LeftDashboard/>
                <RightDashboard/>
            </div>
        );
    }
}

export default DashboardContainer;
