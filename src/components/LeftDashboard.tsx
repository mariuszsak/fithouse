import React from 'react';
import '../shared/Components.scss';
import RoleController from './UserComponents/RoleController';
import AuthService from '../helpers/AuthService';
import {UserRole} from './UserComponents/User';
import MenuDashboard from './LeftDashboardComponents/menuItems/MenuDashboard';
import MenuAdminDashboard from './LeftDashboardComponents/menuItems/MenuAdminDashboard';
import MenuPlan from './LeftDashboardComponents/menuItems/MenuPlan';
import MenuAnCommonItem from './LeftDashboardComponents/menuItems/MenuAnCommonItem';

export interface LeftDashboardState {
    userRole: UserRole | undefined;
}

export class LeftDashboard extends React.Component<{}, LeftDashboardState> {
    private readonly _authService: AuthService = new AuthService();

    constructor(props: {}) {
        super(props);
        this.state = {
            userRole: this._authService.getRoleFromToken()
        };
    }

    render(): JSX.Element {
        return (
            <div className='leftdashdiv'>
                <RoleController for='user'>
                    <MenuDashboard/>
                </RoleController>
                <RoleController for='admin'>
                    <MenuAdminDashboard/>
                </RoleController>
                <RoleController for='user'>
                    <MenuPlan/>
                </RoleController>
                <RoleController for={['admin', 'user']}>
                    <MenuAnCommonItem/>
                </RoleController>
            </div>
        );
    }
}

export default LeftDashboard;
