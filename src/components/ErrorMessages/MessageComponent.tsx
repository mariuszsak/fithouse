import React from 'react';
import '../../shared/ErrorMessages.scss';
import autobind from 'autobind-decorator';
import {
    Alert,
    Button,
    Col,
    Row
} from 'react-bootstrap';

export interface MessageComponentProps {
    history: {
        push(url: string): void;
    };
}

export class MessageComponent extends React.Component<MessageComponentProps, {}> {
    render(): JSX.Element {
        return (
            <div className='container centerHV'>
                <Row>
                    <Col/>
                    <Col>
                        <Alert variant='danger'>
                            Sometching went bad. <br/>
                            Wrong username or password.</Alert>
                        <Button onClick={this.handleClose}>Back to login page</Button>
                    </Col>
                    <Col/>
                </Row>
            </div>
        );
    }

    @autobind
    private handleClose(): void {
        this.props.history.push('/');
    }
}

export default MessageComponent;
