import React from 'react';
import {
    Route,
    Switch
} from 'react-router-dom';
import './App.scss';
import LoginPage from './components/LoginPage';
import DashboardContainer from './components/DashboardContainer';
import RegisterComponent from './components/RegistrationComponents/RegisterComponent';
import RegistrationSuccess from './components/RegistrationComponents/RegistrationSuccess';
import AppContext from './helpers/AppContext';
import {UserRole} from './components/UserComponents/User';
import MessageComponent from './components/ErrorMessages/MessageComponent';

export interface AppState {
    userRole: UserRole;
    setUserRole: (u: UserRole) => void;
}

export class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            userRole: 'user',
            setUserRole: this.setUserRole
        };
    }

    setUserRole = (role: UserRole) => {
        this.setState({
            userRole: role
        });
    };

    render(): JSX.Element {
        return (
            <div className='maindashdiv'>
                <AppContext.Provider value={this.state}>
                    <Switch>
                        <Route exact path='/' component={LoginPage}/>
                        <Route exact path='/dashboard' component={DashboardContainer}/>
                        <Route exact path='/loginError' component={MessageComponent}/>
                        <Route exact path='/register' component={RegisterComponent}/>
                        <Route exact path='/registrationSuccess' component={RegistrationSuccess}/>
                    </Switch>
                </AppContext.Provider>
            </div>
        );
    }
}

export default App;
