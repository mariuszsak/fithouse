import decode from 'jwt-decode';
import {UserRole} from '../components/UserComponents/User';

export class AuthService {
    private static BASE_URL: string = 'http://localhost:3000';

    getLocalStorageToken(): string | null {
        return localStorage.getItem('access_token');
    }

    getRoleFromToken(): UserRole | undefined {
        const role: string | null = localStorage.getItem('role');
        if (role === 'admin') {
            return 'admin';
        }
        if (role === 'user') {
            return 'user';
        }
        return undefined;
    }

    setLocalStorageToken(token: string) {
        localStorage.setItem('access_token', token);
    }

    setRoleFromToken(token: string) {
        const decoded: any = decode(token);
        localStorage.setItem('role', decoded.data.role);
    }

    logout(): void {
        localStorage.removeItem('access_token');
        document.location.href = AuthService.BASE_URL;
    }

    isLocalStorageTokenExpired(token: string | null): boolean {
        try {
            if (!token) {
                return false;
            }
            const decoded: any = decode(token);
            return decoded.exp < Date.now() / 1000;
        } catch (err) {
            return false;
        }
    }
}

export default AuthService;
